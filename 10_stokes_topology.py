from dolfin import *
from ROL.dolfin_vector import DolfinVector as FeVector
import numpy
import ROL

mu = Constant(1.0)                   # viscosity
alphaunderbar = 2.5 * mu / (100**2)  # parameter for \alpha
alphabar = 2.5 * mu / (0.01**2)      # parameter for \alpha
q = Constant(0.5) # q value that controls difficulty/
                  # discrete-valuedness of solution

def alpha(rho):
    """Inverse permeability as a function of rho, equation (40)"""
    return alphabar + (alphaunderbar - alphabar) * rho * (1 + q)/(rho + q)

N = 100
delta = 1.5  # The aspect ratio of the domain, 1 high and \delta wide
V = (1.0/3) * delta  # want the fluid to occupy 1/3 of the domain

mesh = RectangleMesh(Point(0.0, 0.0), Point(delta, 1.0), N, N)
A = FunctionSpace(mesh, "DG", 0) # control function space

U_h = VectorElement("CG", mesh.ufl_cell(), 2)
P_h = FiniteElement("CG", mesh.ufl_cell(), 1)
W = FunctionSpace(mesh, U_h*P_h) # Taylor-Hood function space

class InflowOutflow(Expression):
    def eval(self, values, x):
        values[1] = 0.0
        values[0] = 0.0
        l = 1.0/6.0
        gbar = 1.0

        if x[0] == 0.0 or x[0] == delta:
            if (1.0/4 - l/2) < x[1] < (1.0/4 + l/2):
                t = x[1] - 1.0/4
                values[0] = gbar*(1 - (2*t/l)**2)
            if (3.0/4 - l/2) < x[1] < (3.0/4 + l/2):
                t = x[1] - 3.0/4
                values[0] = gbar*(1 - (2*t/l)**2)

    def value_shape(self):
        return (2,)

def residual(rho, w):
    W = w.function_space()
    (u, p) = split(w)
    (v, q) = TestFunctions(W)
    F = (  alpha(rho) * inner(u, v) * dx 
         + mu * inner(grad(u), grad(v)) * dx 
         + inner(grad(p), v) * dx 
         + inner(div(u), q) * dx 
         + Constant(0) * q * dx
        )
    bc = DirichletBC(W.sub(0), InflowOutflow(degree=2), "on_boundary")
    return (F, bc)

def functional(rho, w):
    (u, p) = split(w)
    J = 0.5 * inner(alpha(rho)*u, u)*dx + mu * inner(grad(u), grad(u))*dx
    return J

def solve_state(rho):
    """Solve the forward problem for a given control rho."""
    w_ = TrialFunction(W)
    (F, bc) = residual(rho, w_)
    w  = Function(W)
    solve(lhs(F) == rhs(F), w, bc)
    return w

def solve_adjoint(rho, w, J):
    (F, bc) = residual(rho, w)
    bc.homogenize() # adjoint has homogeneous BCs

    adj = Function(W)

    adF = adjoint(derivative(F, w))
    dJ = derivative(J, w, TestFunction(w.function_space()))

    solve(action(adF, adj) - dJ == 0, adj, bc)
    return adj

class L2Inner(object):
    def __init__(self):
        self.A = assemble(TrialFunction(A)*TestFunction(A)*dx)

    def eval(self, _u, _v):
        _v.apply('insert')
        A_u = _v.copy()
        self.A.mult(_u, A_u)
        return _v.inner(A_u)

    def riesz_map(self, derivative):
        res = Function(A)
        rhs = Function(A, derivative)
        solve(self.A, res.vector(), rhs.vector())
        return res.vector()

dot_product = L2Inner()

state_file = File("output/08_state.pvd")
control_file = File("output/08_control.pvd")

class ObjR(ROL.Objective):

    def __init__(self, inner_product):
        ROL.Objective.__init__(self)
        self.inner_product = inner_product
        self.rho = Function(A)
        self.state = Function(W)

    def update(self, x, flag, iteration):
        rho = Function(A, x.vec)
        self.rho.assign(rho)
        state = solve_state(self.rho)
        self.state.assign(state)
        if iteration >= 0:
            control_file << self.rho
            state_file << self.state

    def value(self, x, tol):
        J = functional(self.rho, self.state)
        return assemble(J)

    def gradient(self, g, x, tol):
        rho = self.rho
        state = self.state
        lam = solve_adjoint(rho, state, functional(rho, state))

        F = residual(rho, state)[0]
        J = functional(rho, state)
        L = (-action(adjoint(derivative(F, rho)), lam)
            + derivative(J, rho, TestFunction(A)))

        deriv = assemble(L)
        if self.inner_product is not None:
            grad = self.inner_product.riesz_map(deriv)
        else:
            grad = deriv
        g.scale(0)
        g.vec += grad

class VolConstraint(ROL.Constraint):

    def __init__(self, inner_product):
        ROL.Constraint.__init__(self)
        self.inner_product = inner_product

    def value(self, c, x, tol):
        a = Function(A, x.vec)
        val = assemble(a * dx) - V
        c[0] = val

    def applyJacobian(self, jv, v, x, tol):
        da = Function(A, v.vec)
        jv[0] = assemble(da * dx)

    def applyAdjointJacobian(self, ajv, v, x, tol):
        da = TestFunction(A)
        deriv = assemble(da*dx)
        if self.inner_product is not None:
            grad = self.inner_product.riesz_map(deriv)
        else:
            grad = deriv
        ajv.scale(0)
        ajv.vec += grad
        ajv.scale(v[0])

# Initial guess
x = interpolate(Constant(V/delta), A)
x = FeVector(x.vector(), dot_product)

# Bound constraints
lower = interpolate(Constant(0.0), A)
lower = FeVector(lower.vector(), dot_product)
upper = interpolate(Constant(1.0), A)
upper = FeVector(upper.vector(), dot_product)
bound_constraint = ROL.Bounds(lower, upper, 1.0)

# Instantiate Objective class for poisson problem
obj = ObjR(dot_product)

# Tell ROL what space our equality constraint lives in
l = ROL.StdVector(1)
volConstr = VolConstraint(dot_product)

# Turn off PDE solver output
set_log_level(ERROR)

paramsDict = {
   'General': {
       'Secant': {
           'Type': 'Limited-Memory BFGS',
           'Maximum Storage': 5
       } 
   },
   'Step': {
       'Type': 'Augmented Lagrangian',
       'Line Search': {
           'Descent Method': {
               'Type': 'Quasi-Newton Step'
           }
       },
       'Augmented Lagrangian': {
           'Initial Penalty Parameter'               : 1.e3,
           'Penalty Parameter Growth Factor'         : 2,
           'Initial Optimality Tolerance'            : 1.0,
           'Initial Feasibility Tolerance'           : 100.,
           'Print Intermediate Optimization History' : True,
           'Subproblem Step Type'                    : 'Line Search',
           'Subproblem Iteration Limit'              : 10
       }
   },
   'Status Test': {
       'Constraint Tolerance': 1e-4,
       'Gradient Tolerance': 1e-5,
       'Step Tolerance': 1e-8,
       'Iteration Limit': 7
   }
}

params = ROL.ParameterList(paramsDict, "Parameters")
optimProblem = ROL.OptimizationProblem(obj, x,
                bnd=bound_constraint, econ=volConstr, emul=l)
solver = ROL.OptimizationSolver(optimProblem, params)
solver.solve()
